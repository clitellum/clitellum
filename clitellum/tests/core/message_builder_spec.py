from clitellum.core.bus import MessageBuilder, Identification

import unittest

class MessageBuilderTestCase(unittest.TestCase):

    def setUp(self):
        self._identification = Identification('id_servicio', ' tipo_servicio')
        self._body = {'hola' : 'mundo'}
        self._builder = MessageBuilder.create()
        self._header = {'CallContext': {}, 'CallStack': [{'Id' : 'otro_servicio', 'Type' : 'otro_tipo'}]}

    def test_build_message(self):

        message = self._builder.set_service(self._identification) \
                     .set_body(self._body) \
                     .set_type('hola_mundo') \
                     .set_context(self._header) \
                     .build()

        self.assertEqual(message['Body'], self._body)
        self.assertEqual(len(message['Header']['CallStack']), 2)

    def test_build_reply_message(self):

        message = self._builder.set_service(self._identification) \
                     .set_body(self._body) \
                     .set_type('hola_mundo') \
                     .set_context(self._header) \
                     .set_reply() \
                     .build()

        self.assertEqual(message['Body'], self._body)
        self.assertEqual(len(message['Header']['CallStack']), 1)
