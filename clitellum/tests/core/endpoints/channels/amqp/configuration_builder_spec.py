
from clitellum.endpoints.channels.amqp.configuration import AmqpHostConfigurationBuilder, ExchangeType

import unittest
import expects

class AmqpHopstBuilderTestCase(unittest.TestCase):

    def setUp(self):
        self._builder = AmqpHostConfigurationBuilder.create()

    def test_amqp_builder(self):
        expects.expect(self._builder).not_to(expects.be_none)

    def test_out_bound_builder(self):
        self._builder.set_uri('amqp://localhost:5672/clitellumExch')
        self._builder.set_is_out_bound()
        host = self._builder.build()
        expects.expect(host).not_to(expects.be_none)
        expects.expect(host.get_server()).to(expects.equal('localhost'))
        expects.expect(host.get_port()).to(expects.equal(5672))
        expects.expect(host.get_exchange().get_name()).to(expects.equal('clitellumExch'))
        expects.expect(host.get_queue()).not_to(expects.be_none)

    def test_in_bound_builder(self):
        self._builder.set_uri('amqp://localhost:5672/clitellumExch/clitellum_service/Saludos.MensajeSaludo')
        self._builder.set_is_in_bound()
        self._builder.set_topic_exchange()
        host = self._builder.build()
        expects.expect(host).not_to(expects.be_none)
        expects.expect(host.get_server()).to(expects.equal('localhost'))
        expects.expect(host.get_port()).to(expects.equal(5672))
        expects.expect(host.get_exchange().get_name()).to(expects.equal('clitellumExch'))
        expects.expect(host.get_queue().get_name()).to(expects.equal('clitellum_service'))
        expects.expect(host.get_queue().get_routing_keys()).to(expects.have_length(1))
        expects.expect(host.get_queue().get_routing_keys()).to(expects.contain('Saludos.MensajeSaludo'))


    def test_exchange_type_direct(self):
        self._builder.set_uri('amqp://localhost:5672/clitellumExch')
        self._builder.set_is_out_bound()
        self._builder.set_direct_exchange()
        host = self._builder.build()
        expects.expect(host.get_exchange().get_type()).to(expects.equal(ExchangeType.DIRECT))

    def test_exchange_type_topic(self):
        self._builder.set_uri('amqp://localhost:5672/clitellumExch')
        self._builder.set_is_out_bound()
        self._builder.set_topic_exchange()
        host = self._builder.build()
        expects.expect(host.get_exchange().get_type()).to(expects.equal(ExchangeType.TOPIC))

    def test_exchange_create_exchange(self):
        self._builder.set_uri('amqp://localhost:5672/clitellumExch')
        self._builder.set_is_out_bound()
        self._builder.set_create_exchange(True)
        host = self._builder.build()
        expects.expect(host.get_exchange().must_be_created()).to(expects.be_true)

    def test_exchange_create_queue(self):
        self._builder.set_uri('amqp://localhost:5672/clitellumExch/clitellum_service/Saludos.MensajeSaludo')
        self._builder.set_is_in_bound()
        self._builder.set_create_queue(True)
        host = self._builder.build()
        expects.expect(host.get_queue().must_be_created()).to(expects.be_true)

    def test_credentials_connection(self):
        self._builder.set_uri('amqp://localhost:5672/clitellumExch')
        self._builder.set_is_out_bound()
        self._builder.set_credentials('user', 'password')
        host = self._builder.build()
        expects.expect(host.get_user()).to(expects.equal('user'))
        expects.expect(host.get_password()).to(expects.equal('password'))
