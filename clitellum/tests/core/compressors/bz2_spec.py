from clitellum.core.compressors import Bz2Compressor

import unittest

class GzipTestCase(unittest.TestCase):
    """
    Test unitario Aggregate Root
    """
    def setUp(self):
        self._compressor = Bz2Compressor()

    def test_transite_valid_transition(self):
        buffer = "Cadena de prueba para la compresion"
        compressed = self._compressor.compress(buffer)
        decompressed = self._compressor.decompress(compressed)
        self.assertEqual(decompressed, buffer)
