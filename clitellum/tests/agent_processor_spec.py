from clitellum.processors.agents import AgentProcessor
from clitellum.core.bus import Identification
from clitellum.core.eventhandling import EventHook

import unittest
from unittest.mock import Mock, PropertyMock, MagicMock

class AgentProcessorTestCase(unittest.TestCase):

    def setUp(self):
        self._identification = Identification('id', 'service')
        self._sender_gateway = Mock()
        self._receiver_gateway = Mock()

        self._receiver_gateway.OnMessageReceived = Mock()
        self._receiver_gateway.OnMessageReceived.add = Mock()
        self._agent = AgentProcessor(self._identification, self._receiver_gateway, self._sender_gateway)
        self._message = {'hola':'mundo'}

    def test_send_message(self):
        self._agent.send(self._message, 'key')
        self._sender_gateway.send.assert_called_once()
