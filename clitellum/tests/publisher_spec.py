from clitellum.publishers import Publisher
from clitellum.core.bus import Identification

import unittest
from unittest.mock import Mock

class MessageBuilderTestCase(unittest.TestCase):

    def setUp(self):
        self._identification = Identification('id', 'service')
        self._sender_gateway = Mock()
        self._publisher = Publisher(self._identification, self._sender_gateway)
        self._message = {'hola':'mundo'}

    def test_send_message(self):
        self._publisher.publish(self._message, 'key')
        self._sender_gateway.connect.assert_called_once()
        self._sender_gateway.send.assert_called_once()
