# Configuracion entorno de python

Intalación de pipenv
```
pip3 install pipenv
```

Creacion de un entorno virtual de python3
```
pipenv --python 3.6
```

Instalación de todos los requerimientos
```
pipenv install
o
pipenv install --dev
```

Activacion de la consola
```
pipenv shell
```

Ejecución de los test
```
cd tests
pipenv run python runner_test.py
```
