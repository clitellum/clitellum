# Docker

## Generacion de la imagen

```bash
docker build --no-cache -t sbermudezlozano/clitellum_py3 .
docker tag sbermudezlozano/clitellum_py3 sbermudezlozano/clitellum_py3:X.X.X
docker push sbermudezlozano/clitellum_py3:X.X.X
docker push sbermudezlozano/clitellum_py3:latest

```