import yaml
import os
import sys

sys.path.append(os.path.abspath('../../clitellum/'))

import logging
import logging.config
from clitellum import server
from clitellum.handlers import HandlerBase

logcfg = yaml.load(open('logging.yml', 'r'))
logging.config.dictConfig(logcfg)

cfg = yaml.load(open('samurai.yml', 'r'))
srv = server.create_server_from_config(cfg)


@srv.handler_manager.handler('Samurai.Create')
class SamuraiCreateHandler(HandlerBase):
    def __init__(self):
        HandlerBase.__init__(self)

    def handle_message(self, message):
        print ('Creando Samurai: ' + message['samurai'])
        msg = { 'Katana' : { 'Tipo' : 'Chula' } }
        self._send(msg, "Katana.Asignar")

@srv.handler_manager.handler('Katana.Asignada')
class KatanaAsignadaHandler(HandlerBase):
    def __init__(self):
        HandlerBase.__init__(self)

    def handle_message(self, message):
        print (message['Katana'])


srv.start()
input('Pulsa Enter para finalizar')

srv.stop()
