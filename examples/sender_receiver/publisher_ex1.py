
import os
import sys
import yaml

sys.path.append(os.path.abspath('../../clitellum/'))

from clitellum import publishers
import logging
import logging.config


logcfg = yaml.load(open('logging.yml', 'r'))
logging.config.dictConfig(logcfg)

cfg = yaml.load(open('publisher.yml', 'r'))
pb = publishers.create_agent_from_config(cfg)
print ("Publicando el mensaje de saludo")
msg = {'Mensaje' : 'Hola soy el publicador'}

for i in range(0, 1):
    pb.publish(msg, "Saludos.MensajeSaludo")
