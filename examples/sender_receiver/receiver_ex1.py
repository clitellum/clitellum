import yaml
import os
import sys

sys.path.append(os.path.abspath('../../clitellum/'))

import logging
import logging.config
from clitellum import server
from clitellum.handlers import HandlerBase

logcfg = yaml.load(open('logging.yml', 'r'))
logging.config.dictConfig(logcfg)

cfg = yaml.load(open('server.yml', 'r'))
srv = server.create_server_from_config(cfg)


@srv.handler_manager.handler('Saludos.MensajeSaludo')
class MensajeSaludoHandler(HandlerBase):
    def __init__(self):
        HandlerBase.__init__(self)

    def handle_message(self, message):
        print (message['Mensaje'])
        self._send({'Respuesta': 'Mensaje recibido'}, "Respuesta.MensajeSaludo")

srv.start()
input('Pulsa Enter para finalizar')

srv.stop()
