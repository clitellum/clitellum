import os
import sys
import yaml

sys.path.append(os.path.abspath('../../clitellum/'))

import datetime
from clitellum import publishers

# Carga la configuración del publicador desde el fichero publisher.yml
cfg = yaml.load(open('publisher.yml', 'r'))

# Crea el publicador a partir de la configuración
pb = publishers.create_agent_from_config(cfg)

# Crea el mensaje y lo publica
msg = {'datetime' : datetime.datetime.utcnow().isoformat() }
pb.publish(msg, 'HelloWorld.Command')
