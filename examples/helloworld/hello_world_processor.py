import yaml
import os
import sys

sys.path.append(os.path.abspath('../../clitellum/'))

from clitellum import server
from clitellum.handlers import HandlerBase

cfg = yaml.load(open('processor.yml', 'r'))
srv = server.create_server_from_config(cfg)

@srv.handler_manager.handler('HelloWorld.Command')
class HelloWorldCommandHandler(HandlerBase):
    def __init__(self):
        HandlerBase.__init__(self)

    def handle_message(self, message):
        print (message['datetime'].isoformat() + ' - Hello World!! ' )

srv.start()
input('Pulsa Enter para finalizar')

srv.stop()
