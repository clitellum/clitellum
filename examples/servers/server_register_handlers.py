import os
import sys
import yaml

sys.path.append(os.path.abspath('../../clitellum/'))

from clitellum import server
from clitellum.handlers import HandlerBase


__author__ = 'Sergio'

cfg = yaml.load(open('server.yml', 'r'))
srv = server.create_server_from_config(cfg)


@srv.handler_manager.handler('mi_mensaje')
class MiHandler(HandlerBase):

    def __init__(self):
        HandlerBase.__init__(self)


srv.start()

kk = srv.handler_manager.get_handler('mi_mensaje')


del kk

srv.stop()